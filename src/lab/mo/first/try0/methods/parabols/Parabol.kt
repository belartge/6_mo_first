package lab.mo.first.try0.methods.parabols

import lab.mo.first.try0.methods.Algorithm
import lab.mo.first.try0.model.JFunction
import lab.mo.first.try0.model.errors.IterationOverflowException
import lab.mo.first.try0.model.errors.NotCompletedParamsException
import kotlin.math.min
import kotlin.random.Random

class Parabol(val function: JFunction, val precision: Double, val parameters: HashMap<String, Double>) :
    Algorithm(function, precision, parameters) {

    private var deltaMinus = 0.0
    private var deltaPlus = 0.0

    override fun startCalculations() {
        iterations = 0

        checkForParametersCompleted()
        val u1 = parameters["u1"]!!
        val u2 = parameters["u2"]!!
        val u3 = parameters["u3"]!!

        iteration(u1, u2, u3)
    }

    override fun checkForParametersCompleted() {
        if ("u1" !in parameters.keys) {
            throw NotCompletedParamsException("u1")
        }
        if ("u2" !in parameters.keys) {
            throw NotCompletedParamsException("u2")
        }
        if ("u3" !in parameters.keys) {
            throw NotCompletedParamsException("u2")
        }
    }

    private fun checkForIterationOverflow() {
        if (iterations > maxIterations) {
            throw IterationOverflowException(Parabol.name, iterations, "Максимально достигнутое решение: $result")
        }
    }

    private fun calculateDeltas(u1: Double, u2: Double, u3: Double) {
        deltaMinus = function.getValue(u1) - function.getValue(u2)
        deltaPlus = function.getValue(u3) - function.getValue(u2)
    }

    private fun calculateMinimum(u1: Double, u2: Double, u3: Double) {
        val nominator = (u3 - u2)*(u3 - u2) * deltaMinus - (u2 - u1) * (u2 - u1) * deltaPlus
        val denominator = 2 * ((u3 - u2)*deltaMinus + (u2 - u1) * deltaPlus)
//        val nominator =
//            (u2 - u1) * (u2 - u1) * (function.getValue(u2) - function.getValue(u3)) - (u2 - u3) * (u2 - u3) * (function.getValue(
//                u2
//            ) - function.getValue(u1))
//        val denominator =
//            2 * ((u2 - u1) * (function.getValue(u2) - function.getValue(u3)) - (u2 - u3) * (function.getValue(u2) - function.getValue(
//                u1
//            )))
        result = u2 + nominator / denominator
//        result = u2 - nominator / denominator
    }

    private fun checkForConvex(u1: Double, u2: Double, u3: Double): Boolean {
        // проверяет, является ли тройка чисел выпуклой для функции
        calculateDeltas(u1, u2, u3)
        return deltaMinus >= 0 && deltaPlus >= 0 && deltaMinus + deltaPlus > 0
    }

    private fun iteration(u1: Double, u2: Double, u3: Double) {
        checkForIterationOverflow()

        iterations++

        if (checkForConvex(u1, u2, u3)) {
            calculateMinimum(u1, u2, u3)
        }

        if (result < u2) {
            if (function.getValue(result) < function.getValue(u2)) {
                iteration(u1, result, u2)
                return
            }
            if (function.getValue(result) > function.getValue(u2)) {
                iteration(result, u2, u3)
                return
            }
            if (function.getValue(result) == function.getValue(u2)) {
                if (function.getValue(u1) > function.getValue(u2)) {
                    iteration(u1, result, u2)
                    return
                }
                if (function.getValue(u2) > function.getValue(u3)) {
                    iteration(result, u2, u3)
                    return
                }
            }
        }

        if (result > u2) {
            if (function.getValue(result) < function.getValue(u2)) {
                iteration(u2, result, u3)
                return
            }
            if (function.getValue(result) > function.getValue(u2)) {
                iteration(u1, u2, result)
                return
            }
            if (function.getValue(result) == function.getValue(u2)) {
                if (function.getValue(u3) > function.getValue(u2)) {
                    iteration(u2, result, u3)
                    return
                }
                if (function.getValue(u1) > function.getValue(u2)) {
                    iteration(u1, u2, result)
                    return
                }
            }
        }
        if (result == u2) {
            var nu2: Double
            var delta = (u3 - u1) / 10
            do {
                nu2 = if (function.getValue(u2 - delta) < function.getValue(u2 + delta)) {
                    u2 - delta
                } else {
                    u2 + delta
                }
                delta *= 0.1
            } while (!checkForConvex(u1, nu2, u3) && delta >= precision)
            if (delta < precision) {
                calculateDeltas(u1, nu2, u3)
                calculateMinimum(u1, nu2, u3)
            } else {
                iteration(u1, nu2, u3)
            }
        }
    }

    companion object {
        val name: String = "Метод парабол"
    }
}