package lab.mo.first.try0.methods

import lab.mo.first.try0.model.JFunction

abstract class Algorithm(val funtion: JFunction, val epsilon: Double, val params: HashMap<String, Double>) {
    var iterations = 0
    var result: Double = 0.0

    var maxIterations = 100

    abstract fun startCalculations()
    abstract fun checkForParametersCompleted()
}