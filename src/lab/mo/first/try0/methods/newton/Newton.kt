package lab.mo.first.try0.methods.newton

import lab.mo.first.try0.methods.Algorithm
import lab.mo.first.try0.methods.bisection.Bisection
import lab.mo.first.try0.model.JFunction
import lab.mo.first.try0.model.errors.IterationOverflowException
import lab.mo.first.try0.model.errors.NotCompletedParamsException
import lab.mo.first.try0.model.errors.ZeroDivisionException
import java.lang.Math.abs


class Newton(val function: JFunction, val precision: Double, val parameters: HashMap<String, Double>) :
    Algorithm(function, precision, parameters) {

    override fun startCalculations() {
        iterations = 0
        checkForParametersCompleted()
        val u0: Double = parameters["u0"]!!
        iteration(u0)
    }

    override fun checkForParametersCompleted() {
        if ("u0" !in parameters.keys) {
            throw NotCompletedParamsException("u0")
        }
    }

    private fun iteration(u: Double) {
        result = u
        for (iterations in 0 until maxIterations) {
            val diff = JFunction.differentiate(function, 1)
            val f = diff.getValue(result)

            if(abs(f) < precision) {
                this.iterations = iterations
                return
            }

            result -= (f / JFunction.differentiate(diff, 1).getValue(result))
        }
        throw IterationOverflowException(Newton.name, maxIterations, "Максимально достигнутое решение: $result")
    }

    companion object {
        val name: String = "Метод Ньютона"
    }
}