package lab.mo.first.try0.methods.goldensection

import lab.mo.first.try0.methods.Algorithm
import lab.mo.first.try0.model.JFunction
import lab.mo.first.try0.model.errors.IterationOverflowException
import lab.mo.first.try0.model.errors.NotCompletedParamsException
import kotlin.math.sqrt

class GoldenSection(val function: JFunction, val precision: Double, val parameters: HashMap<String, Double>) :
    Algorithm(function, precision, parameters) {

    private val alpha: Double = (sqrt(5.0) - 1) / 2
    private val alpha1: Double = (3 - sqrt(5.0)) / 2

    private var a: Double = 0.0
    private var b: Double = 0.0
    private var u1: Double = 0.0
    private var u2: Double = 0.0

    override fun startCalculations() {
        iterations = 0

        checkForParametersCompleted()
        a = parameters["a"]!!
        b = parameters["b"]!!
        u1 = a + alpha1 * (b - a)
        u2 = a + alpha * (b - a)

        iteration()
    }

    override fun checkForParametersCompleted() {
        if ("a" !in parameters.keys) {
            throw NotCompletedParamsException("a")
        }
        if ("b" !in parameters.keys) {
            throw NotCompletedParamsException("b")
        }
    }

    private fun checkForIterationOverflow() {
        if (iterations > maxIterations) {
            throw IterationOverflowException(GoldenSection.name, iterations, "Максимально достигнутое решение: $result")
        }
    }

    private fun iteration() {
        if (checkForEpsilon()) {
            return
        }
        checkForIterationOverflow()

        iterations++

        val j1 = function.getValue(at = u1)
        val j2 = function.getValue(at = u2)

        if (j1 < j2) {
            b = u2
            u2 = u1
            u1 = a + alpha1 * (b - a)
            iteration()
            return
        }
        if (j1 > j2) {
            a = u1
            u1 = u2
            u2 = a + alpha * (b - a)
            iteration()
            return
        }
        if (j1 == j2) {
            a = u1
            b = u2
            u1 = a + alpha1 * (b - a)
            u2 = a + alpha * (b - a)
            iteration()
            return
        }
    }

    private fun checkForEpsilon(): Boolean {
        result = (b + a) / 2

        if ((b - a) < epsilon) {
            return true
        }
        return false
    }

    companion object {
        val name: String = "Метод Золотого сечения"
    }


}