package lab.mo.first.try0.methods.bisection

import lab.mo.first.try0.methods.Algorithm
import lab.mo.first.try0.model.JFunction
import lab.mo.first.try0.model.errors.IterationOverflowException
import lab.mo.first.try0.model.errors.NotCompletedParamsException

class Bisection(val function: JFunction, val precision: Double, val parameters: HashMap<String, Double>) :
    Algorithm(function, precision, parameters) {

    override fun startCalculations() {
        iterations = 0
        checkForParametersCompleted()
        iteration(parameters["a"]!!, parameters["b"]!!, parameters["sigma"]!!)
    }

    override fun checkForParametersCompleted() {
        if ("a" !in parameters.keys) {
            throw NotCompletedParamsException("a")
        }
        if ("b" !in parameters.keys) {
            throw NotCompletedParamsException("b")
        }
        if ("sigma" !in parameters.keys) {
            throw NotCompletedParamsException("sigma")
        }
    }

    private fun iteration(a: Double, b: Double, sigma: Double) {
        if (checkForEpsilon(a, b)) {
            return
        }
        checkForIterationOverflow()

        iterations++

        val u1 = (b + a - sigma) / 2
        val u2 = (b + a + sigma) / 2

        val j1 = function.getValue(at = u1)
        val j2 = function.getValue(at = u2)

        if (j1 < j2) {
            iteration(a = a, b = u2, sigma = sigma)
            return
        }
        if (j1 > j2) {
            iteration(a = u1, b = b, sigma = sigma)
            return
        }
        if (j1 == j2) {
            iteration(a = u1, b = u2, sigma = sigma)
            return
        }
    }

    private fun checkForIterationOverflow() {
        if (iterations > maxIterations) {
            throw IterationOverflowException(name, iterations, "Максимально достигнутое решение: $result")
        }
    }

    private fun checkForEpsilon(a: Double, b: Double): Boolean {
        result = (b + a) / 2

        if ((b - a) < epsilon) {
            return true
        }
        return false
    }

    companion object {
        val name: String = "Метод деления отрезка пополам"
    }
}