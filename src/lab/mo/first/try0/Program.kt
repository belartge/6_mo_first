package lab.mo.first.try0

import lab.mo.first.try0.methods.bisection.Bisection
import lab.mo.first.try0.methods.goldensection.GoldenSection
import lab.mo.first.try0.methods.newton.Newton
import lab.mo.first.try0.methods.parabols.Parabol
import lab.mo.first.try0.model.JFunction
import lab.mo.first.try0.model.errors.LabException

fun main(args: Array<String>) {
    val list = generateParametersForParabola()

    val parameters = HashMap<String, Double>()
    parameters["a"] = -1.0
    parameters["b"] = 2.0
    parameters["sigma"] = 0.000001
    parameters["u1"] = -1.0
    parameters["u2"] = 0.5
    parameters["u3"] = 2.0
    parameters["u0"] = 3.920940
    val function = JFunction(coeffs = list)

    try {
        val bisection = Bisection(function, precision = 0.0000001, parameters = parameters)
        bisection.startCalculations()

        println("${Bisection.name} ${bisection.result}   ${bisection.iterations}" )

    } catch (e: LabException) {
        println(e.message)
    }

    try {
        val parabol = Parabol(function, precision = 0.0000001, parameters = parameters)
        parabol.startCalculations()
        println("${Parabol.name} ${parabol.result}   ${parabol.iterations}" )

    } catch (e: LabException) {
        println(e.message)
    }

    try {
        val goldenSection = GoldenSection(function, precision = 0.0000001, parameters = parameters)

        goldenSection.startCalculations()
        println("${GoldenSection.name} ${goldenSection.result}   ${goldenSection.iterations}" )

    } catch (e: LabException) {
        println(e.message)
    }

    try {
        val newtone = Newton(function, precision = 0.0000001, parameters = parameters)
        newtone.startCalculations()
        println("${Newton.name} ${newtone.result}   ${newtone.iterations}" )
    } catch (e: LabException) {
        println(e.message)
    }
}

fun generateParametersForTask(): List<Double> {
    val list = ArrayList<Double>()
    list.add(1.0)
    list.add(1.0)
    list.add(0.0)
    list.add(0.0)
    list.add(1.0)
    list.add(1.0)
    return list
}

fun generateParametersForParabola(): List<Double> {
    val list = ArrayList<Double>()
    list.add(1.0)
    list.add(0.0)
    list.add(0.0)
    return list
}

fun generateParametersForAntonTask(): List<Double> {
    val list = ArrayList<Double>()
    list.add(1.0)
    list.add(0.0)
    list.add(0.5)
    list.add(0.0)
    list.add(-2.0)
    list.add(2.0)
    return list
}

fun generateParametersForSashaTask(): List<Double> {
    val list = ArrayList<Double>()
    list.add(0.5)
    list.add(0.25)
    list.add(1.0)
    list.add(0.25)
    list.add(0.5)
    list.add(0.0)
    return list
}

fun generateParametersForNotMyTask(): List<Double> {
    val list = ArrayList<Double>()
    list.add(1.0)
    list.add(0.0)
    list.add(1.0)
    list.add(0.0)
    list.add(-1.0)
    list.add(-2.0)
    return list
}

fun generateEasyList(): List<Double> {
    val list = ArrayList<Double>()
    list.add(1.0)
    list.add(3.0)
    list.add(3.0)
    list.add(1.0)
    return list
}

fun generateSolutedList(): List<Double> {
    val list = ArrayList<Double>()
    list.add(1.0)
    list.add(0.0)
    list.add(-1.0)
    list.add(0.0)
    list.add(0.0)
    list.add(0.0)
    return list
}