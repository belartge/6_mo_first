package lab.mo.first.try0.model

import kotlin.math.pow

class JFunction(val coeffs: List<Double>) {
    fun getValue(at: Double, diffTimes: Int = 0): Double {
        var result = 0.0
        for (i in 0 until coeffs.size) {
            result += coeffs[i] * getDiffCoeff(degree = coeffs.size - 1, diffTimes = diffTimes) * at.pow(coeffs.size - 1 - diffTimes - i)
        }
        return result
    }

    companion object {
        private fun getDiffCoeff(degree: Int, diffTimes: Int): Double {
            var result = 1.0

            if (diffTimes > degree) {
                return 0.0
            }
            if (diffTimes == 0) {
                return 1.0
            }

            for (i in 0 until diffTimes) {
                result *= (degree - i)
            }

            return result
        }

        fun differentiate(function: JFunction, degree: Int = 1): JFunction {
            val coeffs = ArrayList<Double>()
            for (i in 0 until function.coeffs.size - degree) {
                coeffs.add(
                    function.coeffs[i] * getDiffCoeff(
                        degree = function.coeffs.size - 1 - i, diffTimes = degree
                    )
                )
            }
            return JFunction(coeffs)
        }
    }
}