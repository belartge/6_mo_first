package lab.mo.first.try0.model.errors

open class LabException(message: String) : Exception(message) {
    override fun toString(): String {
        return message!!
    }
}