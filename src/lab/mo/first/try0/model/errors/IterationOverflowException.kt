package lab.mo.first.try0.model.errors

class IterationOverflowException(methodName: String, iterations: Int, additionalInfo: String="") :
    LabException("Алгоритм '$methodName' был остановлен на $iterations итерации по причине превышения лимита итераций\n$additionalInfo")