package lab.mo.first.try0.model.errors

class NotCompletedParamsException(parameter: String): LabException("Не указан параметр $parameter")