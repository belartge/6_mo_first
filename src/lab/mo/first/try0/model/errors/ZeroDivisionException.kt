package lab.mo.first.try0.model.errors

class ZeroDivisionException : LabException("Произошло деление на 0")