package lab.mo.first.try0.tests

import lab.mo.first.try0.model.JFunction
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertAll
import kotlin.test.assertEquals

class Shit {
    @Test
    fun test() {
        val function = JFunction(listOf(1.0,2.0,1.0))
        val funced = JFunction.differentiate(function)
        assertEquals(funced.coeffs.toList(), listOf(2.0,3.0))
    }


    @Test
    fun testDifferentiation() {
        val list = ArrayList<Double>()
        list.add(1.0)
        list.add(-2.0)
        //list.add(1.0)
        val function = JFunction(coeffs = list)
        val differed = JFunction.differentiate(function)

        val expected = ArrayList<Double>()
        list.add(2.0)
        // list.add(-2.0)

        MatcherAssert.assertThat(expected, CoreMatchers.equalTo(differed.coeffs))

    }
}